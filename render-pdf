#!/bin/env bash

# Don't clean aux files if rendering fails
set -e

# Set environment variables
TARGET="$1"
TEX_DIR=./.texenv/texlive
TEX_BIN="$TEX_DIR/bin/x86_64-linux"
OUT_DIR=./output
COMPILER=pdflatex
ARGS="-interaction=nonstopmode -shell-escape -output-directory=$OUT_DIR"

# Install TeX environment if missing
if [ ! -d "$TEX_DIR" ]; then
  cd .texenv
  bash install
  cd -
fi

# Render target
if [ ! -d "$OUT_DIR" ]; then
  mkdir "$OUT_DIR"
fi
$TEX_BIN/texliveonfly -c "$COMPILER" -a "$ARGS" --texlive_bin="$TEX_BIN" \
  "$TARGET"

# Clean aux files
$TEX_BIN/latexmk -c "$TARGET" -outdir="$OUT_DIR"
