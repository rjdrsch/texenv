# texenv

Create an isolated environment for any TeX project (inspired by
[TinyTeX](https://yihui.name/tinytex),
[pipenv](https://github.com/pypa/pipenv), and
[renv](https://github.com/rstudio/renv)).

## Setup

To use `texenv`, you'll need to do the following:

* First, create a new, empty directory for your project contents (e.g.,
  `my_project`). Then initialize a new git repository. **NOTE: do not fork or
  clone this repo, as you will not be able to fetch any updates/bug fixes from
  future versions of `texenv`**.

  ```sh
  mkdir my_project # example project directory
  cd my_project
  git init
  ```

* From your project directory, run the following commands:

  ```sh
  git remote add texenv https://gitlab.com/rjustindavis/texenv.git
  git fetch texenv
  git merge texenv/master
  ```

To fetch any upstream changes to `texenv`, simply reenter the following
commands:

```sh
git fetch texenv
git merge texenv/master
```

## Usage

Create your TeX content inside your project folder (`my_project` in the
preceding example). Once you are ready to render to PDF, simply type the
following command into the terminal:

```sh
./render-pdf main.tex
```

If you are rendering for the first time, `texenv` will install a small,
functional TeX environment into the project directory. The TeX environment is
based on [TinyTeX](https://yihui.name/tinytex) and includes many [popular TeX
packages](https://gitlab.com/rjustindavis/texenv/-/blob/master/.texenv/manifest)
along with the base installation. The relevant files are installed in the
hidden folder `.texenv/texlive` and programs can be accessed directly in the
corresponding `.texenv/texlive/bin/*/program`, where `program` is a TeX Live
executable (e.g., `tlmgr`) and `*` depends on your operating system (e.g.,
`x86_64-linux`).

If rendering requires packages that are not in the default TeX environment,
they will attempt to be installed at the time of rendering using the program
`texliveonfly`. Ideally, you should be able to only focus on the content and
let `texenv` create the required distribution.

## Updating the TeX environment

`texenv` is an isolated TeX environment. The project's TeX environment will
retain the same versioning, even if the system-wide TeX distribution updates.
This feature makes projects more secure, in that its markup won't become
obsolete with future package updating. In order words, upgrading the TeX
distribution must be done for the project directly. To update the environment,
navigate to your project directly and run the `update-tex` script:

```sh
cd my_project
./update-tex
```
